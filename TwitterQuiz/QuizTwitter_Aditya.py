#!/usr/bin/env python
# coding: utf-8

# In[1]:


import tweepy
import json

auth = tweepy.OAuthHandler('P4opscCYayfeU7OQFnkqh7WYQ','g2pi4dzjfOYYSoxWyZWHmtlSjUicAizfaKiVM20zvAOQJ8eNo1')
auth.set_access_token('1167359584446513152-HTnVMVfwS0AaJyMLGvmA4genAT2ewO','fnC3UhDvrDox9XGT6pPivRQYk42Wpt8XK7V3VUrzllAFQ')
api = tweepy.API(auth)


# In[4]:


#1. Getting Covid19 and Corona data from twitter
search_terms = ['Covid19','Corona']
def stream_tweets(search_terms):
    data = []
    counter = 0
    
    for tweet in tweepy.Cursor(api.search, q='\"{}\" -filter:retweets'.format(search_term),count=100, lang='en', since='2020-02-11', until='2020-06-10', tweet_mode='extended').items():
        
        tweet_details = {}
        tweet_details['name'] = tweet.user.screen_name
        tweet_details['tweet'] = tweet.full_text
        tweet_details['retweets'] = tweet.retweet_count
        tweet_details['location'] = tweet.user.location
        tweet_details['created'] = tweet.created_at.strftime("%d-%b-%Y")
        tweet_details['followers'] = tweet.user.followers_count
        tweet_details['is_user_verified'] = tweet.user.verified
        data.append(tweet_details)
        
        counter+=1
        if counter == 1000000:
            break
        else:
            pass
    with open('data/{}.json'.format(search_term),'w') as f:
        json.dump(data,f)
    print('done!')


# In[5]:


if __name__ == "__main__":
    print('Starting to stream....')
    for search_term in search_terms:
        stream_tweets(search_term)
    print('finished!')


# In[6]:


import pandas as pd


# In[13]:


#2. Convert JSON to CSV
def json_csv(jsonFileName, csvFileName):

    with open(jsonFileName) as data_file:
        data=json.load(data_file)
    normalized_df = pd.json_normalize(data)
    normalized_df.to_csv(csvFileName,index=False)
    return
json_csv('Covid19.JSON','Covid19.csv')
json_csv('Corona.JSON','Corona.csv')


# In[197]:


#3. Create Bar Cart
import numpy as np
import re


# In[198]:


def clean_tweet(tweet):
    return ' '.join(re.sub('(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)',' ', tweet).split())


# In[204]:


def create_covid19_df():
    covid_df = pd.read_json('data/Covid19.json', orient='records')
    covid_df['clean_tweet'] = covid_df['tweet'].apply(lambda x: clean_tweet(x))
    covid_df['clean_loc'] = covid_df['location'].apply(lambda x: clean_tweet(x))
    covid_df['search_name'] = 'Covid19'
    covid_df.drop_duplicates(subset=['name'], keep='first', inplace=True)
    return covid_df

def create_corona_df():
    covid_df = pd.read_json('data/Corona.json', orient='records')
    covid_df['clean_tweet'] = covid_df['tweet'].apply(lambda x: clean_tweet(x))
    covid_df['clean_loc'] = covid_df['location'].apply(lambda x: clean_tweet(x))
    covid_df['search_name'] = 'Corona'
    covid_df.drop_duplicates(subset=['name'], keep='first', inplace=True)
    return covid_df


# In[205]:


def join_dfs():
    covid19_df = create_covid19_df()
    corona_df = create_corona_df()
    frames = [covid19_df, corona_df]
    searchname_df = pd.concat(frames, ignore_index=True)
    return searchname_df


# In[232]:


def analyze():
    searchname_df = join_dfs()
    covid19_location = create_covid19_df()
    corona_location = create_corona_df()
    covid19_by_location = covid19_location.groupby('clean_loc')['name'].count().reset_index()
    covid19_by_location.columns = ['location_name', 'numbers_of_tweets']
    
    corona_by_location = corona_location.groupby('clean_loc')['name'].count().reset_index()
    corona_by_location.columns = ['location_name', 'numbers_of_tweets']
    
    followers_of_user_by_sn = searchname_df.groupby('search_name')['followers'].mean().reset_index()
    followers_of_user_by_sn.columns = ['keyword_name', 'average_no_of_followers_of_user']
    
    retweets_of_user_by_sn = searchname_df.groupby('search_name')['retweets'].mean().reset_index()
    retweets_of_user_by_sn.columns = ['keyword_name', 'retweets_no_of_followers_of_user']
    
    
    return (covid19_by_location.sort_values(by='numbers_of_tweets',ascending = False).head(3), corona_by_location.sort_values(by='numbers_of_tweets',ascending = False).head(3), followers_of_user_by_sn,retweets_of_user_by_sn)


# In[209]:


from matplotlib import pyplot as plt

def plot_graphs():
    analysis_details = analyze()
    amount_covid19, amount_corona, average_follower, average_retweet = analysis_details
    fig1, ax1 = plt.subplots()
    ax1.bar(amount_covid19['location_name'], amount_covid19['numbers_of_tweets'], label='tweets by location_name')
    ax1.set_xlabel('Location')
    ax1.set_ylabel('Number of tweets')
    ax1.set_title("Amount of 'Covid19' tweet by location")
    
    fig1, ax1 = plt.subplots()
    ax1.bar(amount_corona['location_name'], amount_corona['numbers_of_tweets'], label='tweets by location_name')
    ax1.set_xlabel('Location')
    ax1.set_ylabel('Number of tweets')
    ax1.set_title("Amount of 'Corona' tweet by location")
    
    fig1, ax2 = plt.subplots()
    ax2.bar(average_follower['keyword_name'], average_follower['average_no_of_followers_of_user'], label='tweets by keyword_name')
    ax2.set_xlabel('Keyword')
    ax2.set_ylabel('Average')
    ax2.set_title('Average Follower by Keyword')
    
    fig1, ax2 = plt.subplots()
    ax2.bar(average_follower['keyword_name'], average_retweet['retweets_no_of_followers_of_user'], label='tweets by keyword_name')
    ax2.set_xlabel('Keyword')
    ax2.set_ylabel('Average')
    ax2.set_title('Average Retweets by Keyword')
    
    list_of_figures = [plt.figure(i) for i in plt.get_fignums()]
    return list_of_figures


# In[210]:


plot_graphs()


# In[241]:


#4. Create Cart Line
def create_covid19_df():
    covid_df = pd.read_json('data/Covid19.json', orient='records')
    covid_df['clean_tweet'] = covid_df['tweet'].apply(lambda x: clean_tweet(x))
    covid_df['search_name'] = 'Covid19'
    covid_df.drop_duplicates(subset=['name'], keep='first', inplace=True)
    return covid_df

def create_corona_df():
    covid_df = pd.read_json('data/Corona.json', orient='records')
    covid_df['clean_tweet'] = covid_df['tweet'].apply(lambda x: clean_tweet(x))
    covid_df['search_name'] = 'Corona'
    covid_df.drop_duplicates(subset=['name'], keep='first', inplace=True)
    return covid_df

def analyze():
    covid19_created = create_covid19_df()
    corona_created = create_corona_df()
    covid19_by_created = covid19_created.groupby('created')['tweet'].count().reset_index()
    covid19_by_created.columns = ['created_name', 'numbers_of_tweets']
    
    corona_by_created = corona_created.groupby('created')['tweet'].count().reset_index()
    corona_by_created.columns = ['created_name', 'numbers_of_tweets']
    
    return (covid19_by_created.sort_values(by='created_name',ascending = False).head(3), corona_by_created.sort_values(by='created_name',ascending = False).head(3))

from matplotlib import pyplot as plt

def plot_graphs():
    analysis_details = analyze()
    covid19_by, corona_by = analysis_details
    fig1, ax1 = plt.subplots()
    ax1.plot(covid19_by['created_name'], covid19_by['numbers_of_tweets'], label='tweets by keyword_name')
    ax1.plot(corona_by['created_name'], corona_by['numbers_of_tweets'], label='tweets by keyword_name')
    ax1.set_ylabel('Number of tweets')
    ax1.set_xlabel('Created Time') 
    ax1.set_title('Amount of Tweet by Created Time')
    ax1.legend(['Covid19','Corona'])
    
    list_of_figures = [plt.figure(i) for i in plt.get_fignums()]
    return list_of_figures


# In[242]:


plot_graphs()


# In[ ]:




